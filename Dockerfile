FROM nginx:latest

COPY . /usr/share/nginx/html
COPY config.example.nginx /etc/nginx/conf.d/default.conf

EXPOSE 80
